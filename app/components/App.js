import '../styles/application.scss';
import React, {PureComponent} from 'react';
import io from 'socket.io-client';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowAltCircleUp, faArrowAltCircleDown } from '@fortawesome/free-solid-svg-icons';

let socket = null;

class App extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            check: [],
            time: 0,
        };
        this.output = this.output.bind(this);
        this.createArr = this.createArr.bind(this);
        this.disconect = this.disconect.bind(this);
        this.timeChange = this.timeChange.bind(this);
    }

    componentDidMount() {
        this.output('MSFT');
    }

    createArr() {
        const arr = [...this.state.check];
        arr.push(this.state.data.price);
        if (arr.length > 2) {
            arr.shift();
        }
        this.setState({
            check: arr,
        });
    }

    timeChange(value) {
        this.setState({
            time: value,
        });
    }

    disconect() {
        const req = new XMLHttpRequest();
        socket.disconnect();
        req.open('GET', 'http://localhost:4000/time?time=' + this.state.time);
        req.send();
        req.onload = () => {
            this.output('MSFT');
        };
    }

    output(stockSymbol) {
        socket = io('http://localhost:4000');

        socket.on('connect', () => {
            socket.on(stockSymbol, (data) => {
                this.setState({
                    data: JSON.parse(data),
                }, () => {
                    this.createArr();
                });
            });

            socket.emit('ticker', stockSymbol);
        });
    }

    render() {
        let arrow;
        if (this.state.check[1] > this.state.check[0]) {
            arrow = <FontAwesomeIcon id="green" icon={faArrowAltCircleUp} />;
        } else {
            arrow = <FontAwesomeIcon id="red" icon={faArrowAltCircleDown} />;
        }
        return (
            <div>
                <div className="stock-ticker">
                    <div id="price">
                        <h1>MSFT Price {this.state.data.price}</h1>
                        {arrow}
                    </div>
                    <h3>MSFT Change {this.state.data.change}, {this.state.data.change_percent}%</h3>
                    <h3>MSFT Dividend {this.state.data.dividend}</h3>
                    <h3>MSFT Yield {this.state.data.yield}</h3>
                </div>
                <div>
                    <input id="doTime" onChange={e => this.timeChange(e.target.value)} onKeyUp={e => this.timeChange(e.target.value)} type="number" placeholder="Time in ms (5000)"/>
                    <button id="changeBtn" onClick={this.disconect}>Change Time</button>
                </div>
            </div>
        );
    }
}

export default App;
